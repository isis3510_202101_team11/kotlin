package com.example.loginn.Base

import android.os.Bundle
import androidx.viewbinding.ViewBinding


abstract class BaseViewBindingActivity<VB : ViewBinding>  {

    lateinit var binding: VB


}
