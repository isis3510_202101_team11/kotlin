package com.example.loginn.fragments

import android.app.AlertDialog
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.room.Room
import com.example.loginn.APIs.APITiempos
import com.example.loginn.R
import com.example.loginn.APIs.TiempoResponse
import com.example.loginn.Monitors.NetworkMonitor
import com.example.loginn.SQL.Progress
import com.example.loginn.SQL.ProgressDataBase
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import kotlinx.android.synthetic.main.fragment_tiempo.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.math.roundToInt

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [TiempoFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TiempoFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var listasTiempo = ArrayList<ArrayList<Int>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

        //verifyConnectionToSend()
        //setBarChartValues()
        //mostrarData()
    }

    private fun mostrarData(){
        println("LISTA DE TIEMPOSSS TAMANO GLOBAL "+listasTiempo.size)

        var promedio: ArrayList<Int> = listasTiempo.get(0)
        var minimo: ArrayList<Int> = listasTiempo.get(1)

        //CACHEEE
        val db = Room.databaseBuilder(
            activity!!.applicationContext,
            ProgressDataBase::class.java, "database-progress"
        ).fallbackToDestructiveMigration().build()


        var promMins: Int = promedio.get(0)
        var promSecs: Int = promedio.get(1)
        var minMins: Int = minimo.get(0)
        var minSecs: Int = minimo.get(1)

        var id : Int = (Math.random()*1000).roundToInt()
        var tiempos = Progress(id,promMins, promSecs, minMins, minSecs)
        db.progressDao().insertAll(tiempos)

        recordText.setText("Tiempo record: " + minMins + "'" + minSecs + "''")
        promedioText.setText("Tiempo promedio: " + promMins + "'" + promSecs + "''")

    }

    private fun getCache(){

        CoroutineScope(Dispatchers.IO).launch {
            val db = Room.databaseBuilder(
                activity!!.applicationContext,
                ProgressDataBase::class.java, "database-progress"
            ).fallbackToDestructiveMigration().build()

            var lista : List<Progress> = db.progressDao().getAll()
            var promMins : Int = 0
            var promSecs: Int = 0
            var minMins: Int = 0
            var minSecs: Int = 0

            for (item in lista){
                promMins = item.promMin!!
                promSecs = item.promSec!!
                minMins = item.minMin!!
                minSecs = item.minSec!!
            }


            recordText.setText("Tiempo record: " + minMins + "'" + minSecs + "''")
            promedioText.setText("Tiempo promedio: " + promMins + "'" + promSecs + "''")
            println("mostrando info cacheadaaa")
        }


    }

    private fun getRetrofit():Retrofit{
        return Retrofit.Builder().baseUrl("https://mini-home-lab.herokuapp.com/").addConverterFactory(GsonConverterFactory.create()).build()
    }

    private fun search(){
        CoroutineScope(Dispatchers.IO).launch {
            val call : Response<TiempoResponse> = getRetrofit().create(APITiempos::class.java).getTiempos("tiempos")
            val parejas : TiempoResponse? = call.body()

            if(call.isSuccessful){
                println("FUE EXITOSO")
                val arreglos : List<ArrayList<Int>> = parejas?.tiempos ?: emptyList()
                for(item in arreglos){
                    listasTiempo.add(item)
                }

                mostrarData()
                //show data
            }
            else{
                //show error
                showError()
            }
        }
    }


    fun hasNetwork(context: Context): Boolean? {
        var isConnected: Boolean? = false // Initial Value
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
        if (activeNetwork != null && activeNetwork.isConnected)
            isConnected = true
        return isConnected
    }



    private fun showError(){
        println("error");
    }

    fun setBarChartValues()
    {
        val xValues = ArrayList<String>()

        xValues.add("Lunes")
        xValues.add("Martes")
        xValues.add("Miercoles")
        xValues.add("Jueves")
        xValues.add("Viernes")
        xValues.add("Sabado")
        xValues.add("Domingo")

        val barEntries = ArrayList<BarEntry>()

        barEntries.add(BarEntry(4f,0))
        barEntries.add(BarEntry(3f,1))
        barEntries.add(BarEntry(5.5f,2))
        barEntries.add(BarEntry(4.3f,3))
        barEntries.add(BarEntry(6f,4))
        barEntries.add(BarEntry(2f,5))
        barEntries.add(BarEntry(3.5f,6))

        val barDataSet = BarDataSet(barEntries, "Tiempo promedio")
        barDataSet.color = resources.getColor(R.color.silver)

        val data = BarData(xValues, barDataSet)

        grafica.data = data
        grafica.setBackgroundColor(resources.getColor(R.color.white))
        grafica.animateXY(2000,2000)
    }

    fun verifyConnectionToSend(){
        val networkConnection = NetworkMonitor(activity!!.applicationContext)
        networkConnection.observe(this, Observer { isConnected->
            if(isConnected){

                println("Esta conectado a internet")
            }
            else{
                popConexion(true)
                getCache()
                println("no esta conectado a internet")
            }
        })
    }

    fun popConexion(sendingData: Boolean){
        val builder = AlertDialog.Builder(this.context)
        var sec= "Conexión a la red"
        builder.setTitle(sec)
        builder.setMessage("Hemos detectado que no posee conexión. Verifique su red.")
        builder.setPositiveButton("Aceptar") {dialog,which ->
            dialog.dismiss()
            if(!sendingData){
                //finish()
            }
        }
        val dialog = builder.create()
        dialog.show()
    }



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tiempo, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setBarChartValues()
        if(hasNetwork(activity!!.applicationContext) == true){
            println("HAY INTERNET, peticion base de datos")
            search()
        }
        verifyConnectionToSend()
        //search()
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment TiempoFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            TiempoFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}