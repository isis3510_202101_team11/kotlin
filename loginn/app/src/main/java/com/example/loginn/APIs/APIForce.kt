package com.example.loginn.APIs

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url

interface APIForce {
    @GET
    suspend fun getForces(@Url url:String): Response<ForceResponse>
}