package com.example. loginn

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.*
import android.text.InputType
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.example.loginn.Controllers.SecuenciasController
import com.example.loginn.CustomDialogs.ExplanationDialog
import com.example.loginn.Monitors.NetworkMonitor
import com.example.loginn.databinding.ActivitySecuenciasBinding
import kotlinx.coroutines.*
import java.lang.String
import java.util.concurrent.TimeUnit


class Secuencias:  AppCompatActivity(), SensorEventListener {

    private lateinit var binding: ActivitySecuenciasBinding

    //CONTROLLER
    var controller: SecuenciasController = SecuenciasController();

    //CONEXION
    var isConnected: Boolean =false
    lateinit var networkConnection: NetworkMonitor

    //Sensores
    var sensorManager: SensorManager? =null
    var accelerometro: Sensor?=null
    var vibrador: Vibrator?=null


    //Pusa y cancelar

    var isPaused: Boolean?=false
    var isCanceled: Boolean?=false
    var timeRemaining: Long?=0;

    //Time
    var time:Int=0;

    //Variables de cambio de pos
    var cX : Float?=null
    var cY : Float?=null
    var cZ : Float?=null

    var lX : Float?=null
    var lY : Float?=null
    var lZ : Float?=null


    //Variable verificación para cálculo de aceleración
    var nFT: Boolean?=false
    var cActive: Boolean?=false
    var popActive: Boolean?=false



    //Trigger de vibración
    var shakeT: Float=10f



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySecuenciasBinding.inflate(layoutInflater)
        val bindingRoot = binding.root

        //INFO
        val uno=intent.getStringExtra("exerciseId")
        controller.setid(uno!!)

        //CONNECTIVITY
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        isConnected= activeNetwork?.isConnectedOrConnecting == true

        //MONITOR
        networkConnection=NetworkMonitor(applicationContext)

        //PRIMERO CACHE
        if (!isConnected || controller.getRetrofit()==null){
            if(controller.consultRoutine()!=null){

                setContentView(bindingRoot)
                controller.cacheRoutine()
                setWhole()
                setElements()
                setListeners()
            }
            else{

                setContentView(bindingRoot)
                setWhole()
                binding.inicSec.isClickable=false
                binding.ejerExplicacion.isClickable=false
                popConexion(false)
            }
        }
        else{
            setContentView(bindingRoot)
            GlobalScope.launch {
               controller.getSequence()
                withContext(Dispatchers.Main) {
                    setElements()
                    setListeners()
                }
            }
            setWhole()
        }

    }

    fun setWhole(){
        //NOMBRE
        title = "Mini-HomeLab"

        //Inicio de los botones
        binding.inicSec.visibility=View.VISIBLE
        binding.ejerExplicacion.visibility=View.VISIBLE
        binding.pauseSession.visibility=View.GONE
        binding.abandonarSes.visibility=View.GONE


        //Inicializar el sensor
        sensorManager=getSystemService(Context.SENSOR_SERVICE) as SensorManager
        accelerometro=sensorManager!!.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)

        //Sensor de vibración para movimientos erróneos
        vibrador=getSystemService((Context.VIBRATOR_SERVICE)) as Vibrator
    }

    fun setElements(){
        binding.secName.text = "Secuencia"
        binding.secDesc.text = controller.getSequenceDescrip()
    }

    fun setListeners(){


        //START LA SESIÓN
        binding.inicSec.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {

                //COUNTER
                val timer: CountDownTimer
                //301000
                val millisInFuture: Long = controller.getSequenceTime()
                val countDownInterval: Long = 1000

                //Cambio de visibilidad botones
                binding.inicSec.visibility = View.GONE
                binding.ejerExplicacion.visibility=View.GONE
                binding.pauseSession.visibility = View.VISIBLE
                binding.abandonarSes.visibility = View.VISIBLE

                //PAUSE Y CANCEL
                isPaused = false;
                isCanceled = false;
                cActive = true


                timer = object : CountDownTimer(millisInFuture, countDownInterval) {
                    @SuppressLint("DefaultLocale")
                    override fun onTick(millisUntilFinished: Long) {
                        if (isPaused!! || isCanceled!!) {
                            //If the user request to cancel or paused the
                            //CountDownTimer we will cancel the current instance
                            cancel();
                        } else {
                            val hms = String.format(
                                    "%02d:%02d",
                                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                                            TimeUnit.MILLISECONDS.toHours(
                                                    millisUntilFinished
                                            )
                                    ),
                                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)
                                    )
                            )
                            binding.countTime.text = hms //set text
                            timeRemaining = millisUntilFinished
                            time += 1
                            collectData(time)
                        }
                    }

                    override fun onFinish() {
                        sessionStopped(1, binding.countTime, binding.textoCambiante, v)
                    }
                }.start()
            }
        })

        //PAUSAR LA SESION
        binding.pauseSession.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                if (!isPaused!!) {
                    isPaused = true
                    cActive = false
                    binding.pauseSession.text = "Reanudar sesión"
                } else {

                    //RESUME FUNCTION PERO EN EL MISMO
                    isPaused = false
                    cActive = true
                    binding.pauseSession.text = "Pausar sesión"


                    //Initialize a new CountDownTimer instance

                    //Initialize a new CountDownTimer instance
                    val millisInFuture: Long? = timeRemaining
                    val countDownInterval: Long = 1000
                    object : CountDownTimer(millisInFuture!!, countDownInterval) {
                        @SuppressLint("DefaultLocale")
                        override fun onTick(millisUntilFinished: Long) {
                            //Do something in every tick
                            if (isPaused!! || isCanceled!!) {
                                //If user requested to pause or cancel the count down timer
                                cancel()
                            } else {
                                val hms = String.format(
                                        "%02d:%02d",
                                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                                                TimeUnit.MILLISECONDS.toHours(
                                                        millisUntilFinished
                                                )
                                        ),
                                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                                                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)
                                        )
                                )
                                binding.countTime.text = hms //set text
                                time += 1
                                timeRemaining = millisUntilFinished
                                collectData(time)
                            }
                        }

                        override fun onFinish() {
                            sessionStopped(1, binding.countTime, binding.textoCambiante, v)
                        }
                    }.start()

                }


            }
        })

        //CANCELAR A SESIÓN
        binding.abandonarSes.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                cActive = false
                sessionStopped(3, binding.countTime, binding.textoCambiante, v)
            }
        })


        binding.ejerExplicacion.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                val dialogFragment = ExplanationDialog() // my custom FargmentDialog
                var args= Bundle()
                args.putString("exp", controller.getExplanation());
                args.putString("img", controller.getImg());
                dialogFragment.arguments = args
                if(dialogFragment.arguments!=null){
                    println("NO HAY ARG")
                }
                dialogFragment.show(supportFragmentManager, "Explanation fragment")
            }
        })

    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
        //yeyeyeye
    }

    override fun onSensorChanged(event: SensorEvent?) {

        //Vamos a comenzar con las declaraciones de variables
        //Vamos a guardar unos values, de tal manera que podamos detectar cuando
        //Son 0, ponemos otro mensajito (CAS)
        cX=event!!.values[0]
        cY=event!!.values[1]
        cZ=event!!.values[2]

        if(nFT!!){


            if (((Math.abs(lX!! - cX!!)!! >shakeT && Math.abs(lY!! - cY!!)!! >shakeT) || (Math.abs(lX!! - cX!!)!! >shakeT && Math.abs(lZ!! - cZ!!) >shakeT) || (Math.abs(lY!! - cY!!) >shakeT || Math.abs(lZ!! - cZ!!) >shakeT)) && cActive!!){

                if(!popActive!!){
                    popActive=true
                    popMalEjercicio()

                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    vibrador!!.vibrate(VibrationEffect.createOneShot(800, VibrationEffect.DEFAULT_AMPLITUDE))
                }
            }
        }

        lX=cX
        lY=cY
        lZ=cZ
        nFT=true

    }
    override fun onPause() {
        /*Et on le dés-enregistre quand on sort de l'activité, pour économiser de la batterie*/
        super.onPause()
        sensorManager!!.unregisterListener(this)
    }
    override fun onResume() {
        super.onResume()
        sensorManager!!.registerListener(this, accelerometro, SensorManager.SENSOR_DELAY_NORMAL)
    }


    //POPUP
    fun sessionCancelled(v: View) {
        val alertDialog = AlertDialog.Builder(this).create()
        alertDialog.setTitle("Abandonar sesión")
        alertDialog.setMessage("¿Desea abandonar la sesión? Perderá su progreso.")

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Sí"
        ) { dialog, which ->
            //VIEW
            isCanceled = true
            binding.countTime.text = "00:00"
            time=0;
            timeRemaining=0
            binding.pauseSession.visibility=View.GONE
            binding.abandonarSes.visibility=View.GONE
            binding.inicSec.visibility=View.VISIBLE
            binding.ejerExplicacion.visibility=View.VISIBLE

            //CALIFICACION
            calificacion(v, 3)

            dialog.dismiss() }

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No"
        ) { dialog, which ->
            dialog.dismiss() }
        alertDialog.show()

        val btnPositive = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
        val btnNegative = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE)

        val layoutParams = btnPositive.layoutParams as LinearLayout.LayoutParams
        layoutParams.weight = 10f
        btnPositive.layoutParams = layoutParams
        btnNegative.layoutParams = layoutParams
    }


    ///VERIFICAR CONEXION
    fun verifyConnectionToSend(){
        networkConnection!!.observe(this@Secuencias, Observer { isConnected ->
            if (isConnected) {
                controller.sendData()
                val toast = Toast.makeText(applicationContext, "¡Hemos registrado su rutina!", Toast.LENGTH_LONG)
                toast.show()
                finish()
            } else {
                popConexion(true)
            }
        })
    }


    //POPUP Mal ejercicio

    fun popMalEjercicio(){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Secuencia")
        builder.setMessage("Disminuya la velocidad del ejercicio.")
        builder.setPositiveButton("Aceptar") { dialog, which ->
            popActive=false;
            dialog.dismiss()
        }
        val dialog = builder.create()
        dialog.show()
    }


    //Popup calificación

    fun calificacion(v: View, type: Int){
        val builder: AlertDialog.Builder = android.app.AlertDialog.Builder(this)
        builder.setTitle("Calificación")
        builder.setMessage("¿Qué le pareció este ejercicio?")
        // Set up the input
        val input = EditText(this)
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.hint = "Ingrese la calificación"
        input.inputType = InputType.TYPE_CLASS_NUMBER
        builder.setView(input)

        // Set up the buttons
        builder.setPositiveButton("Enviar", DialogInterface.OnClickListener { dialog, which ->

        })

        val dialog: AlertDialog=builder.create()

        dialog.show()

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(View.OnClickListener {
            var wantToCloseDialog = false
           if (input.text.toString()!=""){
               wantToCloseDialog=true
               var m = input.text.toString().toInt()
               if (type == 1) {
                   controller.catchingSeason(true, m)
               } else if (type == 3) {
                   controller.catchingSeason(false, m)
               }
               dialog.dismiss()
               //VERIFICACIÓN DE CONEXIÓN DESPUÉS DE ESTO
               verifyConnectionToSend()

           }
            else{
               val toast = Toast.makeText(applicationContext, "Por favor ingrese una calificación.", Toast.LENGTH_LONG)
               toast.show()

           }
            if (wantToCloseDialog) dialog.dismiss()
            //else dialog stays open. Make sure you have an obvious way to close the dialog especially if you set cancellable to false.
        })

    }

    //Popup no conexión
    fun popConexion(sendingData: Boolean){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Conexión a la red")
        builder.setMessage("Hemos detectado que no posee conexión. Verifique su red.")
        builder.setPositiveButton("Aceptar") { dialog, which ->
            if(!sendingData){
            finish()
            }
            else{
                binding.abandonarSes.isClickable=false
                binding.pauseSession.isClickable=false
            }
            binding.inicSec.isClickable=false
            dialog.dismiss()
        }
        val dialog = builder.create()
        dialog.show()
    }




    fun collectData(time: Int){
        controller.addData(cX!!, cY!!, cZ!!, time)
    }

    fun sessionStopped(type: Int, countTime: TextView, textoCambiante: TextView, v: View?){
        if(type==1){
            countTime.text = "Fin"
            textoCambiante.text="Ha finalizado la secuencia"
            calificacion(v!!, type)
        }
        else if(type==3){
            sessionCancelled(v!!)
        }
    }
}