package com.example.loginn

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.loginn.databinding.ActivityLevelsBinding
import com.example.loginn.databinding.ActivityRutinaBinding

class LevelsActivity: AppCompatActivity() {

    private lateinit var binding: ActivityLevelsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLevelsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.lvl2.isClickable=false
        binding.lvl3.isClickable=false
        binding.lvl4.isClickable=false
        binding.lvl5.isClickable=false
        binding.lvl6.isClickable=false
    }


    fun displayRoutine(v: View){
        action()
    }

    fun action(){
        startActivity(Intent(this,RutinasActivity::class.java))
    }


}