package com.example.loginn.APIs

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Url

interface APIExercise {

    @GET("/exercises/{id}")
    suspend fun getExercise(@Path("id") id: String): Response<ExerciseResponse>
}