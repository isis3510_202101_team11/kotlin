package com.example.loginn

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        title="Mini-Home lab"

        var dialog = DialogoEstado()
        dialog.show(supportFragmentManager, "dialogoEstado")

        //val objectWatcher: ObjectWatcher = AppWatcher.objectWatcher
    }


    fun displayLevels(v: View){
        action()
    }
    fun displayProgreso(v: View){
        action2()
    }

    fun action(){
        startActivity(Intent(this,LevelsActivity::class.java))
    }

    fun action2(){
        startActivity(Intent(this,Progreso::class.java))
    }
    fun profile(v: View){
        startActivity(Intent(this, Perfil::class.java))
    }

    fun ejerciciosInfo(v: View){
        startActivity(Intent(this,EjerciciosInfo::class.java))
    }


}