package com.example.loginn.APIs

import com.google.gson.annotations.SerializedName

data class ExerciseResponse(
        @SerializedName("Name") val nombre: String,
        @SerializedName("Tiempo") val tiempo: Int,
        @SerializedName("Dificultad") val nivel: Int,
        @SerializedName("Explicacion") val explicacion: String,
        @SerializedName("imgSource") val img:String

)