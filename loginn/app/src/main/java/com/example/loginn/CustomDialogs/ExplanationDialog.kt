package com.example.loginn.CustomDialogs

import android.R.attr.data
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.example.loginn.R
import com.example.loginn.databinding.ExerciseDialogBinding
import com.squareup.picasso.Picasso
import kotlinx.coroutines.coroutineScope


class ExplanationDialog: DialogFragment() {

    private lateinit var binding: ExerciseDialogBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding= ExerciseDialogBinding.inflate(inflater)

        binding.aceptarButton.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                dismiss()
            }
        })

        if (arguments != null) {
            val mArgs = arguments
            var exp= mArgs?.getString("exp")
            var img=mArgs?.getString("img")
            binding.textexpId.text=exp
            Picasso.get().load(img).into(binding.imgExerId);
        }
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        val width = (resources.displayMetrics.widthPixels * 0.85).toInt()
        val height = (resources.displayMetrics.heightPixels * 0.40).toInt()
        dialog!!.window?.setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT)
    }
}