package com.example.loginn.fragments

import android.app.AlertDialog
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.room.Room
import com.example.loginn.APIs.APIRom
import com.example.loginn.APIs.APITiempos
import com.example.loginn.APIs.RomResponse
import com.example.loginn.APIs.TiempoResponse
import com.example.loginn.Monitors.NetworkMonitor
import com.example.loginn.R
import com.example.loginn.SQL.Progress
import com.example.loginn.SQL.ProgressDataBase
import com.example.loginn.SQL.Rom
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import kotlinx.android.synthetic.main.fragment_rom.*
import kotlinx.android.synthetic.main.fragment_tiempo.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.math.roundToInt

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [RomFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class RomFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var listasRom = ArrayList<ArrayList<Int>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

    }

    private fun mostrarData(){
        println("LISTA DE ROMSS TAMANO GLOBAL "+listasRom.size)

        var roms: ArrayList<Int> = listasRom.get(0)

        //CACHEEE
        val db = Room.databaseBuilder(
            activity!!.applicationContext,
            ProgressDataBase::class.java, "database-progress"
        ).fallbackToDestructiveMigration().build()


        var aumentoTotal: Int = roms.get(0)
        var aumentoSemana: Int = roms.get(1)

        var id : Int = (Math.random()*1000).roundToInt()
        var romss = Rom(id, aumentoTotal, aumentoSemana)
        println(""+aumentoTotal + ":::::::"+aumentoSemana)
        db.romDao().insertAll(romss)

        aumentoROMText.setText("Aumento en ROM total: " + aumentoTotal + "%" )
        aumentoSemanaText.setText("Respecto última semana: " + aumentoSemana+ "%" )

    }

    private fun getCache(){

        CoroutineScope(Dispatchers.IO).launch {
            val db = Room.databaseBuilder(
                activity!!.applicationContext,
                ProgressDataBase::class.java, "database-progress"
            ).fallbackToDestructiveMigration().build()

            var lista : List<Rom> = db.romDao().getAll()
            var aumentoTotal : Int = 0
            var aumentoSemana: Int = 0

            for (item in lista){
                aumentoTotal = item.aumentoTotal!!
                aumentoSemana = item.aumentoSemana!!

            }


            aumentoROMText.setText("Aumento en ROM total: " + aumentoTotal + "%" )
            aumentoSemanaText.setText("Respecto última semana: " + aumentoSemana )
            println("mostrando info cacheadaaa ROMMM")
        }


    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder().baseUrl("https://mini-home-lab.herokuapp.com/").addConverterFactory(
            GsonConverterFactory.create()).build()
    }

    private fun search(){
        CoroutineScope(Dispatchers.IO).launch {
            val call : Response<RomResponse> = getRetrofit().create(APIRom::class.java).getRoms("roms")
            val parejas : RomResponse? = call.body()
            println("RESPUESTAAAA : "+parejas)

            if(call.isSuccessful){
                println("FUE EXITOSO")
                val arreglos : List<ArrayList<Int>> = parejas?.roms ?: emptyList()
                for(item in arreglos){
                    listasRom.add(item)
                }

                mostrarData()
                //show data
            }
            else{
                //show error
                showError()
            }
        }
    }

    fun hasNetwork(context: Context): Boolean? {
        var isConnected: Boolean? = false // Initial Value
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
        if (activeNetwork != null && activeNetwork.isConnected)
            isConnected = true
        return isConnected
    }



    private fun showError(){
        println("error");
    }

    fun setBarChartValues()
    {
        val xValues = ArrayList<String>()

        xValues.add("Lunes")
        xValues.add("Martes")
        xValues.add("Miercoles")
        xValues.add("Jueves")
        xValues.add("Viernes")
        xValues.add("Sabado")
        xValues.add("Domingo")

        val barEntries = ArrayList<BarEntry>()

        barEntries.add(BarEntry(25f,0))
        barEntries.add(BarEntry(34f,1))
        barEntries.add(BarEntry(54f,2))
        barEntries.add(BarEntry(23f,3))
        barEntries.add(BarEntry(67f,4))
        barEntries.add(BarEntry(56f,5))
        barEntries.add(BarEntry(12f,6))

        val barDataSet = BarDataSet(barEntries, "")
        barDataSet.color = resources.getColor(R.color.silver)

        val data = BarData(xValues, barDataSet)

        barChart.data = data
        barChart.setBackgroundColor(resources.getColor(R.color.white))
        barChart.animateXY(2000,2000)
    }

    fun verifyConnectionToSend(){
        val networkConnection = NetworkMonitor(activity!!.applicationContext)
        networkConnection.observe(this, Observer { isConnected->
            if(isConnected){

                println("Esta conectado a internet")
            }
            else{
                //popConexion(true)
                getCache()
                println("no esta conectado a internet")
            }
        })
    }

    fun popConexion(sendingData: Boolean){
        val builder = AlertDialog.Builder(this.context)
        var sec= "Conexión a la red"
        builder.setTitle(sec)
        builder.setMessage("Hemos detectado que no posee conexión. Verifique su red.")
        builder.setPositiveButton("Aceptar") {dialog,which ->
            dialog.dismiss()
            if(!sendingData){
                //finish()
            }
        }
        val dialog = builder.create()
        dialog.show()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_rom, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setBarChartValues()
        if(hasNetwork(activity!!.applicationContext) == true){
            println("HAY INTERNET, peticion base de datos")
            search()
        }
        verifyConnectionToSend()
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment RomFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            RomFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}