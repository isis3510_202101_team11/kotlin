package com.example.loginn.APIs

import com.google.gson.annotations.SerializedName

data class TiempoResponse(
    @SerializedName("tiempos")var tiempos:List<ArrayList<Int>>
)