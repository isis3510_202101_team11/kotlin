package com.example.loginn.APIs

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url

interface APIRom {
    @GET
    suspend fun getRoms(@Url url:String): Response<RomResponse>
}