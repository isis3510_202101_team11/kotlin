package com.example.loginn.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.loginn.R
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import kotlinx.android.synthetic.main.fragment_fuerza.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FuerzaFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class FuerzaFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

    }

    fun setBarChartValues()
    {
        val xValues = ArrayList<String>()

        xValues.add("Lunes")
        xValues.add("Martes")
        xValues.add("Miercoles")
        xValues.add("Jueves")
        xValues.add("Viernes")
        xValues.add("Sabado")
        xValues.add("Domingo")

        val barEntries = ArrayList<BarEntry>()

        barEntries.add(BarEntry(5f,0))
        barEntries.add(BarEntry(2.5f,1))
        barEntries.add(BarEntry(6f,2))
        barEntries.add(BarEntry(5.4f,3))
        barEntries.add(BarEntry(3.1f,4))
        barEntries.add(BarEntry(3.2f,5))
        barEntries.add(BarEntry(3.5f,6))
        val barDataSet = BarDataSet(barEntries, "")
        barDataSet.color = resources.getColor(R.color.silver)

        val data = BarData(xValues, barDataSet)

        barChart.data = data
        barChart.setBackgroundColor(resources.getColor(R.color.white))
        barChart.animateXY(2000,2000)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fuerza, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setBarChartValues()
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FuerzaFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FuerzaFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}