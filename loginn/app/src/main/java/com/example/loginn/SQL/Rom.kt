package com.example.loginn.SQL

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Rom (
    @PrimaryKey(autoGenerate = true)
    val rid: Int = -1,
    @ColumnInfo(name = "aumentoTotal") val aumentoTotal: Int?,
    @ColumnInfo(name = "aumentoSemana") val aumentoSemana: Int?
)