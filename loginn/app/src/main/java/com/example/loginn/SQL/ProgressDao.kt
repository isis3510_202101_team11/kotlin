package com.example.loginn.SQL

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface ProgressDao {
    @Query("SELECT * FROM progress")
    fun getAll(): List<Progress>

    @Query("SELECT * FROM progress WHERE pid IN (:progressIds)")
    fun loadAllByIds(progressIds: IntArray): List<Progress>

    @Insert
    fun insertAll(vararg users: Progress)

    @Delete
    fun delete(progress: Progress)
}