package com.example.loginn

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.activity_dialogo_estado.*
import kotlinx.android.synthetic.main.activity_dialogo_estado.view.*

class DialogoEstado : DialogFragment()
{
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var rootView: View = inflater.inflate(R.layout.activity_dialogo_estado, container,false)
        rootView.enviarButton.setOnClickListener{
            val selectedId = radioGroup.checkedRadioButtonId
            val radio = rootView.findViewById<RadioButton>(selectedId)
            isCancelable=false
            if(radio != null )
            {
            var rating = radio.text.toString()
            Toast.makeText(context,"Escogiste: $rating", Toast.LENGTH_SHORT).show()
            dismiss()
            }
            else
            {
                Toast.makeText(context, "No has escogido", Toast.LENGTH_LONG).show()
            }

        }
        return rootView
    }
}