package com.example.loginn.APIs

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import org.json.JSONObject

class Measure(ti: Int,xs: Float, ys: Float, zs: Float) {

    @SerializedName("time")
    @Expose
    private var time: Int = ti

    @SerializedName("x")
    @Expose
    private var x: Float = xs

    @SerializedName("y")
    @Expose
    private var y: Float = ys

    @SerializedName("z")
    @Expose
    private var z: Float = zs


    override fun toString(): String {
        return "[" +
                "{" +
                "time: ${this.time}," +
                "x: ${this.x}," +
                "y: ${this.y}," +
                "z: ${this.z}" +
                "}" +
                "]"
    }

}
