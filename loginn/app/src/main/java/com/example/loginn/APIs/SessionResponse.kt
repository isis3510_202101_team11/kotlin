package com.example.loginn.APIs

import com.google.gson.annotations.SerializedName
import retrofit2.http.Field

data class SessionResponse(
        @SerializedName("sessions") var sessions: Any)
