package com.example.loginn.APIs

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface APIInfoEjercicios {

    @GET("sessions/exercises/{id}")
    suspend fun getInfo(@Path("id") id: String): Response<InfoEjerciciosResponse>
}