package com.example.loginn.APIs

import com.google.gson.annotations.SerializedName

data class InfoEjerciciosResponse(
    @SerializedName("numCompleted") val completados: Int,
    @SerializedName("numNotCompleted") val noCompletados: Int
)
