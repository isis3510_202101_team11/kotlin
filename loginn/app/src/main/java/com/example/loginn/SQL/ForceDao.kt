package com.example.loginn.SQL

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface ForceDao {
    @Query("SELECT * FROM force")
    fun getAll(): List<Force>

    @Query("SELECT * FROM force WHERE fid IN (:forceIds)")
    fun loadAllByIds(forceIds: IntArray): List<Force>

    @Insert
    fun insertAll(vararg forces: Force)

    @Delete
    fun delete(force: Force)
}