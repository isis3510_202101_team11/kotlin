package com.example.loginn.APIs

import com.google.gson.annotations.SerializedName

data class RoutinesResponse(
        @SerializedName("1") val uno: String,
        @SerializedName("2") val dos: String,
        @SerializedName("3") val tres: String,
        @SerializedName("4") val cuatro: String)
