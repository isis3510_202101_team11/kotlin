package com.example.loginn.APIs
import org.json.JSONArray
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.PUT
import retrofit2.http.Path
import java.util.*
import kotlin.collections.ArrayList


interface APISession {

    @PUT("/sessions/{id}")
    @FormUrlEncoded
    fun updateSession(@Path("id") id: String,
                      @Field("ejercicio") ejercicio: String):Call<SessionResponse>
}