package com.example.loginn.APIs

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface APIDias {
    @GET("profiles/last/{id}")
    suspend fun getInfo(@Path("id") id: String): Response<DiasResponse>
}