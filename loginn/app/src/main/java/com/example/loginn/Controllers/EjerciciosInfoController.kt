package com.example.loginn.Controllers

import com.example.loginn.APIs.*
import com.example.loginn.Cache.LRUCache
import com.example.loginn.Cache.PerpetualCache
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_ejercicios_info.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class EjerciciosInfoController {


    var info: InfoEjerciciosResponse?=null
    var id: String? = null
    public var completados : Int = 0
    public var incompletos : Int = 0
    lateinit var collect:ArrayList<Measure>

    companion object Cache: LRUCache(PerpetualCache(),31);


    public fun getRetrofit(): Retrofit {
        return Retrofit.Builder().baseUrl("https://mini-home-lab.herokuapp.com/").addConverterFactory(
            GsonConverterFactory.create()).build()
    }

    suspend fun getSequence(): InfoEjerciciosResponse? {

        val w= CoroutineScope(Dispatchers.IO).launch {

            val call : Response<InfoEjerciciosResponse> = getRetrofit().create(APIInfoEjercicios::class.java).getInfo(id!!)

            val informacion : InfoEjerciciosResponse? = call.body()

            completados=call.body()!!.completados
            incompletos=call.body()!!.noCompletados

            if(call.isSuccessful){

                info=informacion
                completados = info!!.completados
                incompletos = info!!.noCompletados

                if (informacion != null)
                {
                    SecuenciasController[id!!] = informacion
                }
            }
            else
            {
                completados = 0
                incompletos = 0
            }
        }

        w.join()
        return info;
    }

    fun getCompleted(): Int
    {
        return completados
    }

    fun getNoCompleted(): Int
    {
        return incompletos
    }

    fun putId(id: String)
    {
        this.id = id
    }

}