package com.example.loginn.APIs

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class Session(id:String,c: Boolean, f: Int, s:Long, l:Int, d: Long, m: ArrayList<Measure>) {

    @SerializedName("exerciseId")
    @Expose
    public var excerciseId: String = id


    @SerializedName("completed")
    @Expose
    public var completed: Boolean = c

    @SerializedName("feedback")
    @Expose
    public var feedback: Int = f

    @SerializedName("seconds")
    @Expose
    public var seconds: Long = s

    @SerializedName("level")
    @Expose
    public var level: Int = l

    @SerializedName("date")
    @Expose
    public var date: Long = d

    @SerializedName("measures")
    @Expose
    public var measures: ArrayList<Measure> = m




    override fun toString(): String {
        return "{" +
                "completed: ${this.completed}," +
                "feedback: ${this.feedback}," +
                "seconds: ${this.seconds}," +
                "level: ${this.level}," +
                "date: ${this.date}," +
                "measures: ${this.measures.toString()}" +
                "}"
    }

}