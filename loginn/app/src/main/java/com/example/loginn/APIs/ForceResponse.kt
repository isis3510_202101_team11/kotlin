package com.example.loginn.APIs

import com.google.gson.annotations.SerializedName

data class ForceResponse (
    @SerializedName("forces")var forces:List<ArrayList<Int>>
)