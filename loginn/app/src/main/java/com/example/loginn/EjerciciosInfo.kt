package com.example.loginn

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.NetworkInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import android.widget.Toast
import com.example.loginn.Controllers.DiasController
import com.example.loginn.Controllers.EjerciciosInfoController
import com.example.loginn.Monitors.NetworkMonitor
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_ejercicios_info.*
import kotlinx.android.synthetic.main.nointernet.*
import kotlinx.coroutines.*
import java.util.Timer

import androidx.lifecycle.Observer


class EjerciciosInfo : AppCompatActivity() {

    var controllerEj: EjerciciosInfoController = EjerciciosInfoController();
    var controllerDias: DiasController = DiasController()


    var isConnected: Boolean =false
    lateinit var networkConnection: NetworkMonitor

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ejercicios_info)

        //CONNECTIVITY
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        isConnected= activeNetwork?.isConnectedOrConnecting == true

        networkConnection=NetworkMonitor(applicationContext)
        checkConnection()
    }

    private fun checkConnection()
    {
        networkConnection!!.observe(this@EjerciciosInfo, Observer { isConnected ->
            if (isConnected)
            {
                var user = FirebaseAuth.getInstance().currentUser
                var id: String = user.uid

                controllerEj.putId(id)
                controllerDias.putId(id)

                GlobalScope.launch {

                    controllerEj.getSequence()
                    controllerDias.getSequence()
                    withContext(Dispatchers.Main){cargarInfo()}
                }
            }
            else
            {
                val dialog = Dialog(this)
                dialog.setContentView(R.layout.nointernet)
                dialog.setCanceledOnTouchOutside(false)
                dialog.window!!.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT)
                dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                dialog.btnReintentar.setOnClickListener { recreate() }
                dialog.show()
            }
        })
    }
    fun cargarInfo()
    {
        textCompletados.setText(controllerEj.getCompleted().toString())
        textIncompletos.setText(controllerEj.getNoCompleted().toString())
        textUltimavez.setText(controllerDias.getDias().toString())
    }
}