package com.example.loginn.APIs

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Url

interface APIRoutines {
    @GET
    suspend fun getRoutines(@Url url: String): Response<RoutinesResponse>
}