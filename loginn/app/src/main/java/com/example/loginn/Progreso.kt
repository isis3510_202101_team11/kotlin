package com.example.loginn

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.loginn.fragments.FuerzaFragment
import com.example.loginn.fragments.RomFragment
import com.example.loginn.fragments.TiempoFragment
import com.example.loginn.fragments.adapters.ViewPagerAdapter


import kotlinx.android.synthetic.main.activity_progreso.*

class Progreso : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_progreso)

        setUpTabs()
    }

    private fun setUpTabs(){
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(RomFragment(),"ROM")
        adapter.addFragment(FuerzaFragment(), "Fuerza")
        adapter.addFragment(TiempoFragment(), "T. de reacción")
        viewPager.adapter = adapter
        tabs.setupWithViewPager(viewPager)
    }

}