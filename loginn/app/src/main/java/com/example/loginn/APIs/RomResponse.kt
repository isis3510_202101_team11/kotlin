package com.example.loginn.APIs

import com.google.gson.annotations.SerializedName

data class RomResponse (
    @SerializedName("roms")var roms:List<ArrayList<Int>>
)