package com.example.loginn.SQL

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [Progress::class, Force::class, Rom::class], version = 3)
abstract class ProgressDataBase : RoomDatabase() {
    abstract fun progressDao(): ProgressDao
    abstract fun forceDao(): ForceDao
    abstract fun romDao(): RomDao
}