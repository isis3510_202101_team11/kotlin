package com.example.loginn

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.example.loginn.databinding.ActivityForgotPasswordBinding

class ForgotPasswordActivity : AppCompatActivity() {


    private lateinit var txtEmail: EditText
    private lateinit var auth: FirebaseAuth
    private lateinit var progressBar: ProgressBar

    private lateinit var binding: ActivityForgotPasswordBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityForgotPasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)

        title="Mini-Home lab"


        txtEmail = binding.txtEmail
        progressBar = binding.progressBar
        auth= FirebaseAuth.getInstance()
    }

    fun send(view:View){

        val email=txtEmail.toString()

        if(!TextUtils.isEmpty(email)){
            auth.sendPasswordResetEmail(email)
                .addOnCompleteListener(this){
                    task ->
                    if(task.isSuccessful)
                    {
                        progressBar.visibility=View.VISIBLE
                        startActivity(Intent(this,LoginActivity::class.java))
                    }
                    else
                    {
                        Toast.makeText(this, "Error al enviar el email", Toast.LENGTH_SHORT).show()
                    }
                }
        }
    }
}