package com.example.loginn.APIs

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url

interface APITiempos {
    @GET
    suspend fun getTiempos(@Url url:String): Response<TiempoResponse>
}