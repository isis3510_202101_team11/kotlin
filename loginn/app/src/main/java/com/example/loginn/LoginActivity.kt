package com.example.loginn


import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.view.WindowManager
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.nointernet.*
import com.example.loginn.databinding.ActivityLoginBinding

class LoginActivity : AppCompatActivity() {

    private lateinit var txtUser: EditText
    private lateinit var txtPassword: EditText
    private lateinit var progressBar: ProgressBar
    private lateinit var auth:FirebaseAuth

    var isConnected: Boolean =false

    public lateinit var usuario:String
    private lateinit var binding: ActivityLoginBinding



    override fun onCreate(savedInstanceState: Bundle?)
    {

        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        title="Mini-Home lab"

        val manager = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = manager.activeNetworkInfo
        isConnected= networkInfo?.isConnectedOrConnecting == true
        checkConnection()

        txtUser=binding.txtUser
        txtPassword=binding.txtPassword
        progressBar=binding.progressBar

        auth= FirebaseAuth.getInstance()

    }

    fun forgotPassword(view:View){
        startActivity(Intent(this,ForgotPasswordActivity::class.java))
    }

    fun register(view:View){
        startActivity(Intent(this,RegisterActivity::class.java))
    }

    fun login(view:View){
        loginUser()
    }

    private fun loginUser(){

        val user:String=txtUser.text.toString()
        val password:String=txtPassword.text.toString()



        if(!TextUtils.isEmpty(user) && !TextUtils.isEmpty(password)){
            progressBar.visibility=View.VISIBLE

            auth.signInWithEmailAndPassword(user,password)
                .addOnCompleteListener(this){
                    task ->

                    if(task.isSuccessful)
                    {
                        action()
                    }
                    else
                    {
                        Toast.makeText(this, "Error en la autenticación", Toast.LENGTH_SHORT).show()
                    }
                }
        }
    }

    private fun action(){

        startActivity(Intent(this,MainActivity::class.java))
    }

    private fun checkConnection()
    {
        if(!isConnected)
        {
            val dialog = Dialog(this)
            dialog.setContentView(R.layout.nointernet)
            dialog.setCanceledOnTouchOutside(false)
            dialog.window!!.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.btnReintentar.setOnClickListener { recreate() }
            dialog.show()
        }
    }
}