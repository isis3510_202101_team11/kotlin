package com.example.loginn.APIs

import com.google.gson.annotations.SerializedName

data class UOSResponse(
    @SerializedName("numCompleted") val numCompleted: Int,
    @SerializedName("numNotCompleted") val numNotCompleted: Int)
