package com.example.loginn

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.loginn.Controllers.RutinasController
import com.example.loginn.databinding.ActivityRutinaBinding
import com.example.loginn.databinding.ActivitySecuenciasBinding

class RutinasActivity: AppCompatActivity() {

    private lateinit var binding: ActivityRutinaBinding
    val controller:RutinasController=RutinasController()

    var isConnected: Boolean =false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRutinaBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //CONNECTIVITY
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        isConnected= activeNetwork?.isConnectedOrConnecting == true


        binding.sec1.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                displaySecuencia(v!!,"0KTWQ5SMD8QNeSr5How1")
            }
        })

        binding.sec2.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                displaySecuencia(v!!,"QPHqCasFizCEqWBtC9Th")
            }
        })

        binding.sec4.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                displaySecuencia(v!!,"kp2VKkthD3OYsU1nkcDC")
            }
        })

        binding.sec3.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                displaySecuencia(v!!,"ym393RIvArmCZrMrWrUg")
            }
        })
    }


    fun displaySecuencia(v: View, num:String){
        action(num)

    }

    fun action(num:String){
        val intent= Intent(this,Secuencias::class.java)
        intent.putExtra("exerciseId",num)
        startActivity(intent)
    }


}