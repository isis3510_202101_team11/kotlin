package com.example.loginn.SQL

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface RomDao {
    @Query("SELECT * FROM rom")
    fun getAll(): List<Rom>

    @Query("SELECT * FROM rom WHERE rid IN (:romIds)")
    fun loadAllByIds(romIds: IntArray): List<Rom>

    @Insert
    fun insertAll(vararg users: Rom)

    @Delete
    fun delete(rom: Rom)
}