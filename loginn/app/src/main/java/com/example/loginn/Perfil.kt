package com.example.loginn

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_perfil.*


class Perfil : AppCompatActivity() {


    private val db= FirebaseFirestore.getInstance()
    private lateinit var auth: FirebaseAuth



    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_perfil)

        cargarInformacion()
    }

    fun cerrarSesion(v: View)
    {
        startActivity(Intent(this, LoginActivity::class.java))

    }

        fun cargarInformacion()
        {
            var user = FirebaseAuth.getInstance().currentUser

            if (user != null)
            {
                db.collection("profile").document(user.uid).get().addOnSuccessListener{ textMostrarNombre.setText(it.get("Name") as String?)
                    textMostrarEdad.setText(it.get("Edad").toString() as String?)
                    textMostrarPorcentaje.setText(it.get("PorcentajeEjerisicios").toString() as String?)
                }
                db.collection("historial_medico").document(user.uid).get().addOnSuccessListener{ textMostrarEPS.setText(it.get("EPS") as String?)}

            }
        }
}