package com.example.loginn
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.firestore.FirebaseFirestore
import com.example.loginn.databinding.ActivityRegisterBinding

class RegisterActivity : AppCompatActivity() {

    private lateinit var txtName:EditText
    private lateinit var txtEdad:EditText
    private lateinit var txtFecha:EditText
    private lateinit var txtEps:EditText
    private lateinit var txtEmail:EditText
    private lateinit var txtPassword:EditText
    private lateinit var progressBar:ProgressBar
    private lateinit var dbReference: DatabaseReference
    private lateinit var database:FirebaseDatabase
    private lateinit var auth:FirebaseAuth
    private val db= FirebaseFirestore.getInstance()

    private lateinit var binding: ActivityRegisterBinding

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        title="Mini-Home lab"

        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        txtName=binding.txtName
        txtEdad=binding.txtEdad
        txtFecha=binding.txtFecha
        txtFecha.setOnClickListener{showDatePickerDialog()}
        txtEmail=binding.txtEmail
        txtPassword=binding.txtPassword
        txtEps=binding.txtEps
        progressBar=binding.progressBar

        database= FirebaseDatabase.getInstance()
        auth= FirebaseAuth.getInstance()

        dbReference=database.reference.child("User")

    }

    private fun showDatePickerDialog(){
        val datePicker = DatePickerFragment{ day, month, year -> onDateSelected(day, month, year) }
        datePicker.show(supportFragmentManager, "datePicker")
    }

    private fun onDateSelected(day: Int, month: Int, year: Int)
    {
        txtFecha.setText(" $day / $month / $year")
    }

    fun register(view: View){
        createNewAccount()
    }

    private fun createNewAccount(){
        val name:String=txtName.text.toString()
        val edad:String=txtEdad.text.toString()
        val fecha:String=txtFecha.text.toString()
        val email:String=txtEmail.text.toString()
        val password:String=txtPassword.text.toString()
        val eps: String = txtEps.text.toString()

        if(!TextUtils.isEmpty(name) && !TextUtils.isEmpty(eps) && !TextUtils.isEmpty(edad) && !TextUtils.isEmpty(fecha) && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(password))
        {
            progressBar.visibility= View.VISIBLE

            auth.createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener(this)
                {
                    task ->
                    if(task.isComplete)
                    {
                        val user:FirebaseUser?=auth.currentUser
                        verifyEmail(user)

                        val userBD = dbReference.child(user?.uid!!)
                        userBD.child("Name").setValue(name)
                        userBD.child("Edad").setValue(edad)
                        userBD.child("Fecha").setValue(fecha)

                       // db.collection("profile").document(auth.currentUser.uid).set(hashMapOf("Edad" to edad, "FechaNacimiento" to fecha, "Name" to name))
                        //db.collection("historial_medico").document(auth.currentUser.uid).set(hashMapOf("EPS" to eps))

                        auth.currentUser?.let { db.collection("profile").document(it.uid).set(hashMapOf("Edad" to edad, "FechaNacimiento" to fecha, "Name" to name)) }
                        auth.currentUser?.let { db.collection("historial_medico").document(it.uid).set(hashMapOf("EPS" to eps)) }
                        action()
                    }
                }
        }
    }

    private fun action(){
        startActivity(Intent(this,LoginActivity::class.java))
    }

    private fun verifyEmail(user:FirebaseUser?){
        user?.sendEmailVerification()
            ?.addOnCompleteListener(this){
                task->

                if(task.isComplete){
                    Toast.makeText(this, "Email enviado", Toast.LENGTH_SHORT).show()
                }
                else{
                    Toast.makeText(this, "Error al enviar el email", Toast.LENGTH_SHORT).show()
                }
            }
    }
}