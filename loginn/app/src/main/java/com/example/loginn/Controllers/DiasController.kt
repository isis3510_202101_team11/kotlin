package com.example.loginn.Controllers

import com.example.loginn.APIs.APIDias
import com.example.loginn.APIs.DiasResponse
import com.example.loginn.Cache.LRUCache
import com.example.loginn.Cache.PerpetualCache
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class DiasController {



    var info: DiasResponse?=null


    lateinit var id:String

    var ndias: Int = 0


    companion object Cache: LRUCache(PerpetualCache(),31);


    public fun getRetrofit(): Retrofit {
        return Retrofit.Builder().baseUrl("https://mini-home-lab.herokuapp.com/").addConverterFactory(
            GsonConverterFactory.create()).build()
    }

    suspend fun getSequence(): DiasResponse? {

        val w= CoroutineScope(Dispatchers.IO).launch {
            print(id)
            val call : Response<DiasResponse> = getRetrofit().create(APIDias::class.java).getInfo(id!!)

            val informacion : DiasResponse? = call.body()

            if(call.isSuccessful)
            {

                info=informacion
                ndias = informacion!!.dias

                if (informacion != null) {
                    EjerciciosInfoController[id!!] = informacion
                }
            }
            else
            {
                ndias = 0

            }
        }

        w.join()
        return info;
    }

    fun getDias(): Int?
    {
        return ndias
    }

    fun putId(id: String)
    {
        this.id = id
    }


}
