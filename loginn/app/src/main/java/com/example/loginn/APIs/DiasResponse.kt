package com.example.loginn.APIs

import com.google.gson.annotations.SerializedName

data class DiasResponse(
    @SerializedName("lastDaysCount")var dias:Int
)
