package com.example.loginn.SQL

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Force (
    @PrimaryKey(autoGenerate = true)
    val fid: Int = -1,
    @ColumnInfo(name = "aumentoTotal") val aumentoTotal: Int?,
    @ColumnInfo(name = "aumentoMes") val aumentoMes: Int?
)
