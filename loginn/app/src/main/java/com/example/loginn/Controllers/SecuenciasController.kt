package com.example.loginn.Controllers

import androidx.appcompat.app.AppCompatActivity
import com.example.loginn.APIs.*
import com.example.loginn.Cache.LRUCache
import com.example.loginn.Cache.PerpetualCache
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*

class SecuenciasController: AppCompatActivity() {

    //ALMACENAR RUTINAS
    var r: ExerciseResponse?=null
    var id:String?=null
    lateinit var collect:ArrayList<Measure>

    //SINGLETON, PARA QUE SE CONSERVE
    companion object Cache: LRUCache(PerpetualCache(),21);


    public fun getRetrofit(): Retrofit {
        return Retrofit.Builder().baseUrl("https://mini-home-lab.herokuapp.com/").addConverterFactory(GsonConverterFactory.create()).build()
    }

    suspend fun getSequence(): ExerciseResponse?{

        val w=CoroutineScope(Dispatchers.IO).launch {
            val call : Response<ExerciseResponse> = getRetrofit().create(APIExercise::class.java).getExercise(id!!)

            val routine : ExerciseResponse? = call.body()

            if(call.isSuccessful){

                r=routine
                collect= ArrayList()
                if (routine != null) {
                    Cache[id!!] = routine
                }

            }
            else{

            }
        }

        w.join()
        return r;
    }

    fun setid(i:String){
        id=i
    }


    fun getSequenceTime():Long{
        var f= r!!.tiempo * 1000
        return f.toLong()
    }

    fun getExplanation(): String{
        return r!!.explicacion
    }

    fun getImg(): String{
        return  r!!.img
    }

    fun getSequenceDescrip():String{
        return r!!.nombre
    }

    fun consultRoutine():Any?{
        val w=Cache[id!!]
        return w
    }

    fun cacheRoutine(){
        val u= Cache[id!!] as ExerciseResponse
        r=u
        collect= ArrayList()
    }

    fun addData(xs:Float, ys:Float, zs:Float, t:Int){
        val d=Measure(t,xs,ys,zs)
        collect.add(d)
    }

    fun catchingSeason(completed:Boolean,feedback:Int){
        val currentTimestamp = System.currentTimeMillis()
        val f=Session(id!!,completed,feedback,r!!.tiempo.toLong(),r!!.nivel,currentTimestamp,collect)
        val gSon = Gson().toJson((f))
        Cache["finalSession"]=gSon
    }

    fun sendData(){
            CoroutineScope(Dispatchers.Default).launch {
                val collecting = getRetrofit().create(APISession::class.java)
                    .updateSession("vE49a9nqoHCemBjcHv0v", Cache["finalSession"].toString())
                collecting.enqueue(object : Callback<SessionResponse> {
                    override fun onFailure(call: Call<SessionResponse>, t: Throwable) {
                        //Igual responderá esto porque como tal el put no estoy respondiendo

                        Cache.remove("finalSession")
                    }

                    override fun onResponse(
                        call: Call<SessionResponse>,
                        response: Response<SessionResponse>
                    ) {
                    }

                })


            }
}}