package com.example.loginn.Controllers

import com.example.loginn.APIs.APIRoutines
import com.example.loginn.APIs.ExerciseResponse
import com.example.loginn.APIs.RoutinesResponse
import kotlinx.coroutines.*
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.ArrayList

class RutinasController {

    var r: RoutinesResponse?=null
    var state:String?=null

    public fun getRetrofit(): Retrofit {
        return Retrofit.Builder().baseUrl("https://mini-home-lab.herokuapp.com/").addConverterFactory(GsonConverterFactory.create()).build()
    }

    suspend fun getRecommededExercises(){
        runBlocking {
            val w = async(context = Dispatchers.IO) {
                val call: Response<RoutinesResponse> =
                    getRetrofit().create(APIRoutines::class.java).getRoutines("Exercises/estado")
                val routines: RoutinesResponse? = call.body()
                if (call.isSuccessful) {
                    r = routines
                }
            }
            w.join()
        }
    }

    fun getUno():String{
        return r!!.uno
    }
    fun getDos():String{
        return r!!.dos
    }

    fun getTres():String{
        return r!!.tres
    }

    fun getCuatro():String{
        return r!!.cuatro
    }

    fun setState(s: String):String{
        state=s
        println(state)

        return state as String
    }
}