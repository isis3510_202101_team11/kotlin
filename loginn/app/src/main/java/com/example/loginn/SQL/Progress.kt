package com.example.loginn.SQL

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Progress (

    @PrimaryKey(autoGenerate = true)
    val pid: Int = -1,
    @ColumnInfo(name = "promMin") val promMin: Int?,
    @ColumnInfo(name = "promSec") val promSec: Int?,
    @ColumnInfo(name = "minMins") val minMin: Int?,
    @ColumnInfo(name = "minSec") val minSec: Int?


)